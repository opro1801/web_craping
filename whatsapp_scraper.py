#imports here
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
import pandas as pd
import re
import sqlite3

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)

#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:/Users/LAU/Desktop/fb/chromedriver.exe', chrome_options=chrome_options)

#open the webpage
driver.get("http://www.facebook.com")

#target username
username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))

#enter username and password
username.clear()
username.send_keys("fortestgithub@gmail.com")
password.clear()
password.send_keys("Github123")

#target the login button and click it
button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

#We are logged in!

link = "https://www.facebook.com/ifood.hk"
time.sleep(5)
fb_link = pd.read_excel('list_fb.xlsx', index_col=0)

ws = {'Company':[], 'Facebook':[] ,'Whatsapp':[], 'Phone': [], 'Messenger':[]}
count = 0

'''elements = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 embtmqzv fe6kdd0r mau55g9w c8b282yb hrzyx87i m6dqt4wy h7mekvxk hnhda86s oo9gr5id hzawbc8m']")
driver.get(link)
print(elements[0].find_elements_by_tag_name("span")[0].text)'''


for link in fb_link[0]:
	#count+=1
	#if(count == 10):
	#	break

	ws['Facebook'].append(link)
	driver.get(link)
	time.sleep(5)

	elements = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 embtmqzv fe6kdd0r mau55g9w c8b282yb hrzyx87i m6dqt4wy h7mekvxk hnhda86s oo9gr5id hzawbc8m'")
	comp_name = elements[0].find_elements_by_tag_name("span")[0].text
	if(len(comp_name) == 0):
		comp_name = 'na'
	ws['Company'].append(comp_name)

	#Finding whatsapp number
	elements = driver.find_elements_by_css_selector("a[class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 pq6dq46d p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l cbu4d94t taijpn5t k4urcfbm']")
	pattern = '852[0-9]{8}'
	elements = [a.get_attribute('href') for a in elements]
	elements = [a for a in elements if 'whatsapp' in a]
	whatsapp_number = 'na'
	if len(elements) != 0:
		whatsapp_number = re.findall(pattern, elements[0])[0]
	ws['Whatsapp'].append(whatsapp_number)
	
	#print(whatsapp_number)

	#Finding phone number
	elements = driver.find_elements_by_css_selector("span[class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh jq4qci2q a3bd9o3v knj5qynh oo9gr5id']")
	if(len(elements) == 0): continue
	elements = [a.text for a in elements]
	pattern1 = '[0-9]{4}\s[0-9]{4}'
	pattern2 = '[0-9]{8}'
	elem = 'na'
	for e in elements:
		if re.fullmatch(pattern1, e) != None or re.fullmatch(pattern2, e) != None:
			elem = e
			break
	ws['Phone'].append(elem)

	#Finding messenger link
	pattern3 = 'https://www.facebook.com/messages/t/'
	pt = 'https://www.facebook.com/'
	ws['Messenger'].append(pattern3+link[len(pt):])

	
	#break
df = pd.DataFrame(ws)

con = sqlite3.connect('result.db')
cur = con.cursor()

cur.execute('''CREATE TABLE Information
				(Company text, Facebook text, Whatsapp text, Phone text, Messenger text)''')
cur.execute('''CREATE TABLE Result
				(Company text, Facebook text, Whatsapp text, Phone text, Messenger text)''')

df.to_sql('Information', con, if_exists='append', index = False)

cur.execute('''INSERT INTO Result
	SELECT DISTINCT * FROM Information''')
cur.execute('''DROP TABLE Information''')

con.commit()
con.close()
